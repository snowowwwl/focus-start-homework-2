#Реализовать все шаги

dvc run -n base_part \
        -d ../services/base_part.py \
        -o ../data/base_part.fthr \
        python -u ../services/base_part.py
        
dvc run -n feature_time \
        -d ../services/feature_name_1.py \
        -o ../data/feature_time.fthr \
        python -u ../services/feature_name_1.py
	
dvc run -n feature_weather \
        -d ../services/feature_name_2.py \
        -o ../data/feature_weather.fthr \
        python -u ../services/feature_name_2.py
	
dvc run -n model \
        -d ../services/base_part.py -d ../services/feature_name_1.py  -d ../services/feature_name_2.py \
        -o ../data/model.pkl \
        python -u ../services/model.py
