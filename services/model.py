# Здесь нужно:
#1 объединить все насчитанные признаки с основной частью;
#2 Обучить любую модель, на любом кол-ве данных - качество не играет никакой роли 
# (пример можно поискать тут:https://www.kaggle.com/c/ashrae-energy-prediction/notebooks);
#3 Сохранить модель в pickle.

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import catboost as ctb
from catboost import CatBoostRegressor
import pickle

df = pd.read_feather('../data/base_part.fthr')
f1 = pd.read_feather('../data/feature_time.fthr')
f2 = pd.read_feather('../data/feature_weather.fthr')
df = df.set_index('bid_meta').join(f1.set_index('building_id'))
df = df.set_index('site_id').join(f2.set_index('sid_meta'))
df.dropna(inplace=True)

#Ограничила данные , так как комп не тянет
y = df["meter_reading"].iloc[0 : 10000]
X = df.drop(["meter_reading"], axis=1).iloc[0 : 10000]

X_train_part, X_valid, y_train_part, y_valid = train_test_split(X, y, test_size=0.3, random_state=2020)

#CatboostRegressor
ctb = CatBoostRegressor(random_seed=17)
cat_features_idx = np.where(df.dtypes == 'object')[0].tolist()
ctb.fit(X_train_part, y_train_part, logging_level='Silent', cat_features=cat_features_idx)
ctb_valid_pred = ctb.predict(X_valid)
print(np.sqrt(mean_squared_error(y_valid, ctb_valid_pred)))

# save to pickle
with open('../data/model.pkl','wb') as f:
    pickle.dump(ctb,f)
