# Нужно реализовать насчет признак в рамках вашей гипотезы №1

import pandas as pd
from helpers import send_sql_query
import sys
import psycopg2


def get_feature_time():
    """Извлекаем из базы данных фичи времени
    """
    return send_sql_query("""
    SELECT 
        building_id,
        date_part('day', timestamp_measurement) as day,
        date_part('year', timestamp_measurement) as year,
        date_part('month', timestamp_measurement) as month,
        date_part('hour', timestamp_measurement) as hour
    FROM measurement_results
    WHERE date_part('day', timestamp_measurement) = 30 AND
          date_part('year', timestamp_measurement) = 2016 AND
          date_part('month', timestamp_measurement) = 12
    LIMIT 10000
    """)
df = get_feature_time()
print(df.shape)
df.to_feather('../data/feature_time.fthr')
