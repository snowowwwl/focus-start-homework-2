# Нужно реализовать насчет признак в рамках вашей гипотезы №2

import pandas as pd
from helpers import send_sql_query
import sys
import psycopg2


def get_feature_weather():
    """Извлекаем из базы данных фичи:
    	- скорость ветра выше среднего (1) или ниже среднего значения(0)
    	- температура воздуха ниже среднего (1) или выше(0)
    """
    return send_sql_query("""
    SELECT 
        site_id as sid_meta,
        wind_speed,
        CASE 
          WHEN wind_speed >= 3.6 THEN 1   
          WHEN wind_speed < 3.6 THEN 0   
        END  
        as greatermeanwind,
        air_temperature,
        CASE 
          WHEN air_temperature <= 14.5 THEN 1   
          WHEN air_temperature > 14.5 THEN 0   
        END  
        as lessmeantemp
    FROM weather_train
    WHERE date_part('day', timestamp_measurement) = 30 AND
          date_part('year', timestamp_measurement) = 2016 AND
          date_part('month', timestamp_measurement) = 12
    LIMIT 10000
    """)
df = get_feature_weather()
print(df.shape)
df.to_feather('../data/feature_weather.fthr')
