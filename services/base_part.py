import pandas as pd
from helpers import send_sql_query
import sys
import psycopg2


def get_base_part():
    """Реализуйте функцию которая будет извлекать из базы данных 
    
    1. Целевую переменную; (meter_reading)
    2. Идентификаторы для объединения с другими признаками; (building_id, meter)
    3. Время измерения. (timestamp_measurement)
    """
    return send_sql_query("""
    WITH main_table as (
    SELECT 
        building_id as bid_meta,
        meter,
        meter_reading
    FROM measurement_results
    WHERE date_part('day', timestamp_measurement) = 30 AND
          date_part('year', timestamp_measurement) = 2016 AND
          date_part('month', timestamp_measurement) = 12
    ),
    
    buildings as (
    SELECT
        building_id,
        site_id
    FROM building_metadata
    )
    SELECT * 
	FROM main_table as mt
	LEFT JOIN buildings as b ON b.building_id = mt.bid_meta
	LIMIT 10000
    """)
df = get_base_part()
print(df.shape)
df.to_feather('../data/base_part.fthr')
